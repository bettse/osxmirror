//
//  main.m
//  osxmirror
//
//  Created by Eric Betts on 2/11/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>
#include <stdlib.h>

#define TAG_IN 1
#define TAG_OUT 2
#define UPSIDE_DOWN 5
#define RIGHTSIDE_UP 4

NSString* bytesToString (uint8_t* bytes, CFIndex length)
{
    NSMutableString *result = [NSMutableString stringWithCapacity:2*length + length - 1];

    for (int i = 0; i < length; i++) {
        [result appendFormat:@"%02X", bytes[i]];
    }

    return [result lowercaseString]; // Result is auto-released
}

void runScript(NSString* tagID, int direction)
{
    NSString* strDirection;
    NSString* script;
    NSTask *task = [NSTask new];
    NSArray *arguments;
    NSFileManager *filemgr = [NSFileManager defaultManager];

    if (direction == TAG_IN){
        strDirection = @"IN";
        NSLog(@"Tag %@ %@", tagID, strDirection);
    }else if (direction == TAG_OUT){
        strDirection = @"OUT";
        NSLog(@"Tag %@ %@", tagID, strDirection);
    } else {
        NSLog(@"unknown event %X: %@", direction, tagID);
        return;
    }

    NSString* curDir = [[NSFileManager defaultManager] currentDirectoryPath];

    script = [NSString stringWithFormat:@"%@/%@", curDir, tagID];

    if ([filemgr fileExistsAtPath: script] == NO) {
        script = [NSString stringWithFormat:@"%@/default", curDir];
        if ([filemgr fileExistsAtPath: script] == NO) {
            NSLog(@"Coun't find specific or default script for tag");
            return;
        }
    }

    arguments = [NSArray arrayWithObjects: tagID, strDirection, nil];

    [task setLaunchPath: script];
    [task setArguments: arguments];
    [task setStandardInput:[NSPipe pipe]];
    [task setStandardOutput:[NSPipe pipe]];

    [task launch];
    [task waitUntilExit];
    NSLog(@"Terminationstatus: %d", [task terminationStatus]);
}

void MyInputCallback(void *context, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength)
{
    // process device response buffer (report) here
    NSString* tagID = @"";
    int interface = report[0];
    int method = report[1];
    int correlationID = report[2] * 255 + report[3];
    int dataLength = report[4];
    uint8_t* data = report + 5;
    if(reportLength == 0 || report[1] == 0) {
        return;
    }else if(reportLength == 64){
        switch (interface) {
            case 1:
                switch (method) {
                    case UPSIDE_DOWN:
                        NSLog(@"Uh oh!  Mir:ror upside down");
                        break;
                    case RIGHTSIDE_UP:
                        NSLog(@"Phew!  Mir:ror rightside up");
                        break;
                }
                break;
            case 2:
                tagID = bytesToString(data, dataLength);
                runScript(tagID, method);
                break;
            default:
                NSLog(@"Unknown interface %i, method %i, cid %i", interface, method, correlationID);
                NSLog(@"data [%@]", bytesToString(data, dataLength));
                break;
        }
    }
}


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        IOReturn sendRet;
        const long vendorId = 0x1da8;
        const long productId = 0x1301;
        size_t bufferSize = 64;
        char *inputBuffer = malloc(bufferSize);
        char *outputBuffer = malloc(bufferSize);
        memset(outputBuffer, 0, bufferSize);
        NSMutableArray *pattern = [[NSMutableArray alloc] init];

        //0) get colors from command line
        for(int i = 1; i < argc; i++){
            [pattern addObject:[NSNumber numberWithInt:atoi(argv[i])]];
        }

        /*
         *
         * This code borrows heavily (pretty much completely) from http://osdir.com/ml/usb/2009-09/msg00019.html
         *
         */

        //1) Setup your manager and schedule it with the main run loop:

        IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault,
                                                        kIOHIDOptionsTypeNone);
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetMain(),
                                        kCFRunLoopDefaultMode);
        IOHIDManagerOpen(managerRef, 0L);

        //2) Get your device:


        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[NSNumber numberWithLong:productId] forKey:[NSString
                                                                    stringWithCString:kIOHIDProductIDKey encoding:NSUTF8StringEncoding]];
        [dict setObject:[NSNumber numberWithLong:vendorId] forKey:[NSString
                                                                   stringWithCString:kIOHIDVendorIDKey encoding:NSUTF8StringEncoding]];
        IOHIDManagerSetDeviceMatching(managerRef, (__bridge CFMutableDictionaryRef)dict);
        NSSet *allDevices = (__bridge NSSet *)(IOHIDManagerCopyDevices(managerRef));
        NSArray *deviceRefs = [allDevices allObjects];
        IOHIDDeviceRef deviceRef = ([deviceRefs count]) ? (IOHIDDeviceRef)CFBridgingRetain([deviceRefs objectAtIndex:0]) : nil;
        //NSLog(@"deviceRef = %p", deviceRef);

        //3) Setup your buffers (I'm making the assumption the input and output buffer sizes are 64 bytes):
        IOHIDDeviceRegisterInputReportCallback(deviceRef, (uint8_t *)inputBuffer,
                                               bufferSize, MyInputCallback, NULL);

        //4) Send your message to the device (I'm assuming report ID 0):

        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        NSString* curDir = [[NSFileManager defaultManager] currentDirectoryPath];
        NSString* nochoreo = [NSString stringWithFormat:@"%@/.nochoreo", curDir];

        if ([filemgr fileExistsAtPath: nochoreo] == YES) {
            //Interface
            outputBuffer[0] = 0x03;
            //Method
            outputBuffer[1] = 0x01;
            //CID
            outputBuffer[2] = 0;
            outputBuffer[3] = 0;
            //length
            outputBuffer[4] = 0;
            sendRet = IOHIDDeviceSetReport(deviceRef, kIOHIDReportTypeOutput, 0, (uint8_t *)outputBuffer, bufferSize);
            NSLog(@"Choreography off");
        }else{
            //NSLog(@"Choreography on");
        }
        //5) Enter main run loop (which will call MyInputCallback when data has come back from the device):
        NSLog(@"osxmirror loaded");
        [[NSRunLoop mainRunLoop] run];
    }
    return 0;
}

